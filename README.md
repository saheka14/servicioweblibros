# README #

### WEB API PARA CATALOGO DE LIBROS ###

* Este servicio web sirve de backend a la AppLibros. 
* Version 1.1


## Instrucciones de desarrollo##
 
* 1. Tomar los scripts del archivo V1.0.0__baseline.SQL y ejecutarlos en el gestor de base de datos de POSTGRES mediante PGADMIN.
* 2. Compilar la aplicación con Visual Studio Code , descargar dependencias con npm install y ejecutarla, por default corre en el puerto 8080.
 
## Dependencias: ##

* Node.js v16.13.0
* Postgres PostgreSQL 14.0

##Funciones del API.##

#1. http://localhost:8080/libros/get

     * Descripcion: Obtiene el listado del Catálogo de libros.
     * Metodo POST
     * Seguridad mediante Header Authorization Basic YWRtaW46bm9raWEzMjIw
     * Ejemplo Body: {"searchs":"El viejo y el mar"}

#2. http://localhost:8080/libros/add

	 * Descripcion: Inserta un nuevo libro en la base de datos.
	 * Metodo POST
     * Seguridad mediante Header Authorization Basic YWRtaW46bm9raWEzMjIw
     * Ejemplo Body: { "nuevo_libro": { "libro_nombre":"Los hornos de hitler","libro_autor":"Olga Lengyel","libro_año":"1947"}}

#3. http://localhost:8080/libros/login
	 
	 * Descripcion: Sirve para validación inicial de login.
	 * Metodo POST
     * Ejemplo Body: { "usuario":"admin","password":"nokia3220"}

