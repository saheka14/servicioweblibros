
---------------------------------------------------------------------------------------------------------------------------------------------------------------
--Extension encrypt/desencrypt password.
---------------------------------------------------------------------------------------------------------------------------------------------------------------
--btoa('admin:nokia3220')
--Basic YWRtaW46bm9raWEzMjIw
--{"searchs":"El viejo y el mar"}
--{ "nuevo_libro": { "libro_nombre":"Los hornos de hitler","libro_autor":"Olga Lengyel","libro_año":"1947"}}


CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE DATABASE LibrosBD;
--Tablas

CREATE TABLE usuarios (
	US_idUsuario SERIAL		PRIMARY KEY,
	US_usuario VARCHAR(40)	NOT NULL,
	US_password BYTEA		NOT NULL,
	US_estatus	VARCHAR(1)		NOT NULL
);

CREATE TABLE libros(
	LI_idLibro SERIAL PRIMARY KEY,
	LI_nombre VARCHAR(50) NOT NULL,
	LI_autor VARCHAR(50) NOT NULL,
	LI_año VARCHAR(4) NOT NULL,
	LI_estatus VARCHAR(1) NOT NULL
);
---------------------------------------------------------------------------------------------------------------------------------------------------------------
--Insertar datos prueba
---------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO libros ( LI_nombre, LI_autor , LI_año , LI_estatus ) VALUES
	('El viejo y el mar',	    'Ernest Hemingway',			'1952',	'A'),
	('Cien años de soledad',	'Gabriel Garcia Marquez',	'1967',	'A'),	
    ('Pedro Paramo',		    'Juan Rulfo',				'1955',	'A'),
	('Poema de Gilgamesh',	    'Anonimo',			        '1800',	'A'),
	('Crimen y castigo',	    'Fiódor Dostoievski',	    '1866',	'A'),	
	('Educacion sentimental',	'Gustave Flaubert',	        '1869',	'A'),
    ('El homre invicible',		'Ralph Ellison',			'1952',	'A');


INSERT INTO usuarios (US_usuario , US_password,US_estatus) 
SELECT 'admin', pgp_sym_encrypt('nokia3220', 'admin', 'compress-algo=1, cipher-algo=aes256'), 'A' ;
INSERT INTO usuarios (US_usuario , US_password,US_estatus) 
SELECT 'test', pgp_sym_encrypt('nokia3220', 'test', 'compress-algo=1, cipher-algo=aes256'), 'A' ;
---------------------------------------------------------------------------------------------------------------------------------------------------------------
--#############################################################################################################################################################
--Functions
---------------------------------------------------------------------------------------------------------------------------------------------------------------
--Validación de credenciales
---------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION login(key_user VARCHAR(40), pswd_user TEXT)
   RETURNS TABLE (status Boolean, "message" TEXT, code INT)
AS $$
BEGIN
	CREATE TABLE tempAccount AS
    SELECT US_idUsuario FROM users u
	WHERE u.US_usuario = key_user AND pgp_sym_decrypt(US_password, US_usuario) = pswd_user AND US_estatus = 'A';

	IF (SELECT COUNT(*) = 1 FROM tempAccount) THEN
		RETURN QUERY SELECT TRUE, 'OK', 200;
	ELSIF (SELECT COUNT(*) = 0 FROM tempAccount) THEN
		RETURN QUERY SELECT FALSE, 'Usuario o password incorrecto', 401;
	ELSE
		RETURN QUERY SELECT FALSE, 'Hay mas de una cuenta con las mismas credenciales', 409;
	END IF;

	DROP TABLE tempAccount;
END; $$
LANGUAGE 'plpgsql';
--------------------------------------
 --Agregar nuevo libro
--------------------------------------

CREATE OR REPLACE FUNCTION add_libros(libro_json JSON)
    RETURNS TABLE(status BOOLEAN, "message" TEXT)
AS $$
DECLARE str_nombre VARCHAR(50);
DECLARE str_autor VARCHAR(50);
DECLARE str_año VARCHAR(4);

BEGIN
	SELECT 
	x->>'libro_nombre', 
	x->>'libro_autor', 
	x->>'libro_año'
	INTO str_nombre, str_autor,str_año
	FROM json_array_elements(libro_json) x;

	IF EXISTS(SELECT FROM libros WHERE LI_nombre = str_nombre AND LI_estatus <> 'E') THEN
		RETURN QUERY SELECT FALSE, 'El libro ya esta dado de alta.';
	ELSE
		INSERT INTO libros (LI_nombre,  LI_autor,   LI_año, LI_estatus) VALUES (str_nombre, str_autor, str_año, 'A');
		RETURN QUERY SELECT TRUE,'OK';
	END IF;
END; $$
LANGUAGE 'plpgsql';

------------------------------------
-- Consultar libros
------------------------------------
CREATE OR REPLACE FUNCTION get_libros(searchs VARCHAR(50) = null )
    RETURNS JSON
AS $$
DECLARE str_query TEXT;
DECLARE json_response JSON;
BEGIN
	str_query = 'SELECT ARRAY_TO_JSON(COALESCE(ARRAY_AGG(t), ''{}'')) FROM (';
	str_query = str_query || 'SELECT '||
				'LI_idLibro "id", '||
				'LI_nombre "nombre", '||
				'LI_autor "autor", '||
				'LI_año "anio", '||
				'LI_estatus "estatus" '||
				'FROM libros ';
				
				IF (searchs <> '') THEN
				
					str_query = str_query || ' WHERE LI_nombre like ''%'||searchs||'%'' ';
					str_query = str_query || ' OR LI_autor like ''%'||searchs||'%'' ';
					str_query = str_query || ' OR LI_año like ''%'||searchs||'%'' ';
			
				END IF;	
	EXECUTE (str_query || ') t;') INTO json_response;
	RETURN json_response;
END; $$
LANGUAGE 'plpgsql';