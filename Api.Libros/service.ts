'use strict'

import './src/model/tools/string.extensions';
import { Usuario } from "./src/model/Usuario";
import { Response } from "./src/model/objects/Response";
import { LibrosService } from "./src/model/libros/LibrosService";

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const PACKAGE_JSON = require('./package.json');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.post('/libros/login/', async function (req, res) {
    let response = new Response();
    try
    {
            response = await new Usuario().login(Object.assign({}, req.body));
    }catch(err)
    {
        response.http_code = 500;
        response.body.set(false,err.message);
    }
    res.status(response.http_code).send( deepClone( response.body ));
});


app.post('/libros/add/', async function (req, res) {
    let response = new Response();
    try
    {
        let credentials = req.headers.authorization;
        response = await new Usuario().authentication(credentials);
        if( response.body.status )
        {
            response = await new LibrosService().add(Object.assign({}, req.body));
        }
    }catch(err)
    {
        response.http_code = 500;
        response.body.set(false,err.message);
    }
    res.status(response.http_code).send( deepClone( response.body ));
});

app.post('/libros/get/', async function (req, res) {
    let response = new Response();
    try
    {
        let credentials = req.headers.authorization;
        response = await new Usuario().authentication(credentials);
        if( response.body.status )
        {
            response = await new LibrosService().get(Object.assign({}, req.body));
        }
    }catch(err)
    {
        response.http_code = 500;
        response.body.set(false,err.message);
    }
    res.status(response.http_code).send( deepClone( response.body ));
});


function deepClone(object: Object) : Object {
    return JSON.parse(JSON.stringify(object));
}

function sendResponse(res:any,response:Response )
{
    if(response.http_code == 200){
        res.status(200).send( deepClone( response.body.data));
    }else{
        res.status(response.http_code).send(response.body.message);
    }
}

app.listen(8080, function () {
    console.log(`[SUCCESS] - Servicio web Libros inicia en puerto 8080.`);});