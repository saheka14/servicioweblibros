interface String {
  parse(type): string;
}

String.prototype.parse = function (type: string): any {
  switch (type) {
    case 'number':
      return parseFloat(this);
    case 'string':
      return this;
    case 'boolean':
    return (this == 'true');
    case 'object':
      return JSON.parse(this);
    default:
      return undefined;
  }
}
  