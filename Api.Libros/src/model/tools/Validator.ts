const RequestValidator = require('request-validator-csima');
const regex = require('./../tools/regex.json');

const customerIntegrationKeyRgx = 'sensitive_key2';
const integrationServiceKeyRgx = 'sensitive_key';
const accountKeyRgx = 'sensitive_key2';
const endpointRgx = 'url';
const assetKeyRgx = 'sensitive_key2';
const searchRgx = 'sensitive_key2';

export class LibrosServiceValidator {
  //public response : Response;
  //public jsonValidation: Object;

  public add(params) {
    let resp_val = new RequestValidator({
      language: "english",
      regex: regex
    }).validate(params, [{
      id: "nuevo_libro",
      type: "object",
      properties: [ {
        id: 'libro_nombre',
        type: 'string'
      }, {
        id: 'libro_autor',
        type: 'string'
      }, {
        id: 'libro_año',
        type: 'string'
      }
      ]
    }]);
    return resp_val;
  }
  public get(params) {
    let jsonValidation: Object;
    let resp_val;

        jsonValidation = [{
          id: "search",
          type: "string",
          regex_type: searchRgx,
          allow_undefined: true
        }]

    resp_val = new RequestValidator({ languague: "english", regex: regex }).validate(params, jsonValidation);
    return resp_val;
  }
  public login(params){
    let jsonValidation: Object;
    let resp_val;

        jsonValidation = [{
          id: "usuario",
          type: "string"
        },{
          id:"password",
          type:"string"
        }]

    resp_val = new RequestValidator({ languague: "english", regex: regex }).validate(params, jsonValidation);

    return resp_val;
  }
}