const RequestValidator = require('request-validator-csima');
const regex = require('./../tools/regex.json');

import { LibrosServiceAD } from "./AD/LibrosServiceAD";
import { Response } from "../objects/Response";
import { LibrosServiceValidator as Validator } from "../tools/Validator"
import { isBreakOrContinueStatement } from "typescript";

export class LibrosService {

    async add(params) {
        let response = new Response();
        let validator = new Validator();
        let resp_val = validator.add(params);

        if (resp_val.status) {
            let resp_add = await new LibrosServiceAD().add(params.nuevo_libro);
            response.set(200, resp_add.status, resp_add.message);
        }
        else {
            response.set(400, false, resp_val.message);
        }
        return response;
    }
    
    async get(params) {
        let response = new Response();
        let validator = new Validator();
        let resp_val = validator.get(params);
        if (resp_val.status) {
            let resp = await new LibrosServiceAD().get(params.searchs);
            
            let items = {
                items: resp[0]['get_libros']
            };

            response.set(200, true, 'OK', items);
        } else {
          response.set(400, false, resp_val.message);
        }
        return response;
    }

}