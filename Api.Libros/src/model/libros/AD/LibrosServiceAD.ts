import internal from "node:stream";
import { PostgressConnection } from "../../../conection/PostgressConnection";

export class LibrosServiceAD {

    public get(searchs: string) {
        const connection = new PostgressConnection();
        return connection.callFunction('get_libros',
        [searchs]
        ).catch(err => {
            throw err;
        });
    }

    public add(nuevo_libro: Object) {
        const connection = new PostgressConnection();
        return connection.callFunction('add_libros', [JSON.stringify([nuevo_libro])]).then(data => {
            return data[0];
        }).catch(err => {
            throw err;
        });
    }
}