import { Response } from "./objects/Response";
import { UserAD } from "./AD/UserAD";
import { LibrosServiceValidator as Validator } from "./tools/Validator";



export class Usuario
{
    public async authentication(auth: string) {
        let _response = new Response();
        try {

            if(auth == undefined) 
            {
                _response.http_code = 400;
                _response.body.set(false,'El header de autorizacion no esta incluido');

            }else{

                let arrAuth: string[] = Buffer.from(auth.replace("Basic ", ""), 'base64').toString().split(":");

                if(arrAuth.length == 2)
                {
                    //Llamada a funcion postgress
                    let response_function = await new UserAD().login(arrAuth[0],arrAuth[1]);
                    _response.http_code = response_function.code;
                    _response.body.set(response_function.status,response_function.message);
                }
                else{
                  
                    _response.body.set(false,'Las credenciales no tienen el formato correcto');
                    _response.http_code = 400;
                }
            }
        } catch (err) 
        {
            throw err;
        }

        return _response;
    }
    public async login(params) {
        let _response = new Response();
        try {

           /* if(auth == undefined) 
            {
                _response.http_code = 400;
                _response.body.set(false,'El header de autorizacion no esta incluido');

            }else{

                let arrAuth: string[] = Buffer.from(auth.replace("Basic ", ""), 'base64').toString().split(":");

                if(arrAuth.length == 2)
                {*/
                    //Llamada a funcion postgress
                    let response = new Response();
                    let validator = new Validator();
                    let resp_val = validator.login(params);
            
                    if (resp_val.status) {

                    
                    let response_function = await new UserAD().login(params.usuario,params.password);
                    _response.http_code = response_function.code;
                    _response.body.set(response_function.status,response_function.message);
                    }
                    else {
                        _response.body.set(false, resp_val.message);
                        _response.http_code = 400;
                    }
              /*  }
                else{
                  
                    _response.body.set(false,'Las credenciales no tienen el formato correcto');
                    _response.http_code = 400;
                }
            }*/
        } catch (err) 
        {
            throw err;
        }

        return _response;
    }
}