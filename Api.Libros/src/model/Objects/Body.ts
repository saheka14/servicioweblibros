export class Body {
    public status: boolean = true;
    public message: string = "OK";
    public data: object;

    public set(status: boolean, message: string, data: object = null) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}