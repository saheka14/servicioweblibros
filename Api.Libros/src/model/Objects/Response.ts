import { Body } from './Body';

export class Response {


    public http_code: number;
    public body: Body = new Body();

    public set(http_code: number, status: boolean, message:string, data: Object = null)
    {
        this.http_code = http_code;
        this.body.set(status, message, data);
    }

}