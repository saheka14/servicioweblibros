const pg = require('pg-promise')();
let client = null;
import { Config } from "../../Config";
import  "../extensions/String";

export class PostgressConnection {

    public constructor() {
        var strConn = this.getStrConnection();
        if (strConn) {
            if (client == null) {
                client = pg(strConn);
            }
        }
    }

    public close() {
        if (client != null) {
            client.$pool.end();
            client = null;
        }
    }

    public async callFunction(functionName: string, params: any[]) {
        return await client.func(functionName, params);
    }

    private getStrConnection(): string {
        return "{0}://{1}:{2}@{3}:{4}/{5}".format(
            Config.key("DbType"), Config.key("DbUser"), Config.key("FLYWAY_PASSWORD"),
            Config.key("DbAddress"), Config.key("DbPort"), Config.key("DbName")
        );
    }
}
