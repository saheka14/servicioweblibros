const config = require('./config.json');

export class Config {

    public static config(): any {
        return config.configuration;
    }

    public static key(key: string): string {
        return process.env[key] || config.keys[key];
    }
}